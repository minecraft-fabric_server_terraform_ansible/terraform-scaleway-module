variable "scaleway_provider_region" {}
variable "scaleway_provider_zone" {}

variable "scaleway_project_id" {}


variable "scaleway_extra_volume_size" {}
variable "scaleway_extra_volume_type" {}


variable "scaleway_security_group_inbound_default_policy" {}
variable "scaleway_security_group_outbound_default_policy" {}
variable "scaleway_security_group_ssh_port" {}
variable "scaleway_security_group_minecraft_port" {}
variable "scaleway_security_group_geyser_port" {}


variable "scaleway_instance_server_type" {}
variable "scaleway_instance_server_image" {}
variable "scaleway_instance_server_root_volume_persistent" {}