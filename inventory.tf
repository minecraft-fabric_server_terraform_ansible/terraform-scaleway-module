resource "local_file" "inventory" {
    content = templatefile(
        "${path.module}/inventory.tftpl",
        { 
            hostnames         = scaleway_instance_server.vpc.id,
            ansible_hosts     = scaleway_instance_ip.public_ip.address
        }
    )
    filename = "dynamic_inventory"
}