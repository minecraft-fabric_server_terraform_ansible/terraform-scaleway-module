terraform {
  required_version = ">= 0.13.0"

  required_providers {
    scaleway ={
      source    = "scaleway/scaleway"
    }
  }
}

provider "scaleway" {
  zone = var.scaleway_provider_zone
  region = var.scaleway_provider_region
}

resource "scaleway_instance_ip" "public_ip" {
  project_id = var.scaleway_project_id
}

resource "scaleway_instance_volume" "data" {
  project_id = var.scaleway_project_id
  size_in_gb = var.scaleway_extra_volume_size
  type = var.scaleway_extra_volume_type
}

resource "scaleway_instance_security_group" "main" {
  project_id = var.scaleway_project_id
  inbound_default_policy = var.scaleway_security_group_inbound_default_policy
  outbound_default_policy = var.scaleway_security_group_outbound_default_policy

  inbound_rule {
    action = "accept"
    port = var.scaleway_security_group_ssh_port
    protocol = "TCP"
  }

  inbound_rule {
    action = "accept"
    port = var.scaleway_security_group_minecraft_port
    protocol = "TCP"
  }

  inbound_rule {
    action = "accept"
    port = var.scaleway_security_group_geyser_port
    protocol = "UDP"
  }

  outbound_rule {
    action = "drop"
    ip_range = "10.20.0.0/24"
  }
}

resource "scaleway_instance_server" "vpc" {
  project_id = var.scaleway_project_id
  type = var.scaleway_instance_server_type
  image = var.scaleway_instance_server_image
  
  ip_id = scaleway_instance_ip.public_ip.id
  additional_volume_ids = [ scaleway_instance_volume.data.id ]
  security_group_id= scaleway_instance_security_group.main.id

  root_volume {
    delete_on_termination = var.scaleway_instance_server_root_volume_persistent
  }
}